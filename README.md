# Hotbar Transfer

## Description
The purpose of this script was to generate a macro to be copy pasted from the generated txt file to a FFXIV macro to transfer the hotbars from one crafting profession to all others. By default it will copy the Weaver hotbars to each one taking macro palletes 1-4.

## Use
```js
node craftingmacro.js
```

## License
    This script generates a list of commands to transfer the hot bars from one crafting proffession to others.
    Copyright (C) 2022  Jeremy Bikadi

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Project status
This project has been completed and achieves the original goal set out for it.


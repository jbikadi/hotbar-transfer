const fs = require('fs');

const jobs = ['bsm', 'alc', 'crp', 'gsm', 'cul', 'arm', 'ltw', 'wvr'];
const copyFrom = 'wvr';

const macro = jobs.filter(x => x !== copyFrom).map( function(e) {
    const r = [];
    for (let i = 1; i <= 4; i++) {
        r.push(`/hotbar copy ${copyFrom} ${i} ${e} ${i}`);
    }
    return r.join('\n');
}).join('\n');

console.log(macro);
fs.writeFile('macro.txt', macro, (err) => ( (err) ? (console.log(err)) : (console.log('-- file saved --') )) );